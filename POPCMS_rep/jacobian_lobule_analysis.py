# -*- coding: utf-8 -*-
"""
Created on Thu Oct  4 15:03:28 2018

@author: MS
"""

import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
import pandas as pd
import os
import scipy.io as scio
from sklearn.metrics import auc
from sklearn.model_selection import train_test_split
import math

root = 'CEREBELLA/'
suit_path = root + 'Atlases/Cerebellum-SUIT.nii' #path for labelled atlas
volume = nib.load(suit_path) #load atlas
vol_data = volume.get_data()
dist_vals = 70 #number of distinct lobules within the posterior fossa

[rows,cols,slices] = vol_data.shape
w = rows*cols*slices  #find total number of voxels in atlas


# splitting into left and right
zero_idxs = (vol_data == 0)
#originally atlas does not distinguish between left and right lobules and only contains 35 distinct labels.
#The volume is split into two and the labels on the left side are double in value, resulting in 70 distinct lobules
vol_data[0:70, :, :] = vol_data[0:70, :, :] + 35
vol_data[zero_idxs] = 0
#splitting over


vrow = vol_data.reshape(w,1)#vectorise atlas

idxlist = list();

# find distinct values
#DV = np.unique(vrow)
DV = range(dist_vals)
for val in DV:
    idx = np.where(vrow == val)
    idxlist.append(idx)

ds = pd.read_excel( root + 'DS.xlsx')
dates_all = { 'SA' : ds["A"] - ds["Date of Surg"] , 'SB' : ds["B"] - ds["Date of Surg"] , 'SC' : ds["C"] - ds["Date of Surg"] , 
              'SD' : ds["D"] - ds["Date of Surg"] , 'SE' : ds["E"] - ds["Date of Surg"] }

dall = pd.DataFrame(dates_all,columns= ['SA', 'SB', 'SC', 'SD', 'SE'])


#Find group label
PFS_specific = ds["POPCMS"]
 
months = 7
counter = 0
group = float('nan') * np.ones((48,1))
count_files = 0


scan_codes = ['A', 'B', 'C', 'D', 'E']
data_j = float('nan') * np.ones((48,(len(scan_codes)* dist_vals)))
data_int_j = float('nan') * np.ones((48,(months* dist_vals)))

file_list = os.listdir(root + 'Deformation Fields/')

for p in range(48): #iterate through each patient
    for sc in range(len(scan_codes)): #iterate through scans A to E
        
        keyword = 'mf' + str(p+1) + scan_codes[sc]
        for fname in file_list:
            if keyword in fname:
                TX_path = root + 'Deformation Fields/' + fname #path to registered image

        if os.path.exists(TX_path):
            
            print("Processing image file: ", TX_path, " ...")
            TX_struct = scio.loadmat(TX_path) #load deformation field
            
            TX = (TX_struct["Tr"])
            
            s = TX.shape
            s1 = s[0]*s[1]*s[2]*s[3] #establish idex increments for Jaxobian of deformations determinant function
            s2 = s1/3
            s2 = int(s2)
    
            Tx = np.zeros((3,s2))
    
            tx0 = TX[:,:,:,0]
            tx1 = TX[:,:,:,1]
            tx2 = TX[:,:,:,2]
    
            Tx[0,:] = tx0.reshape((1,s2))
            Tx[1,:] = tx1.reshape((1,s2))
            Tx[2,:] = tx2.reshape((1,s2))
    
            dv = np.zeros((s[0]*s[1]*s[2],1))
    
            c=-1
            y_step = s[0]
            z_step = s[0] * s[1]
    
            for x in range(s[2]):
                for y in range(s[1] - 1):
                    for z in range(s[0] - 1):
                
                        c = c+ 1
                
                        if ((x==s[0])or(y==s[1])or(z==s[2])):
                            
                            dv[c] = 1.0
                
                        else:
                            b1 = ( 4 - (Tx[0,c]+Tx[0,c+y_step]+Tx[0,c+z_step]+Tx[0,c+y_step+z_step]-Tx[0,c]-Tx[0,c+y_step]-Tx[0,c+z_step]-Tx[0,c+y_step+z_step]))
                            b2 = ( 4 - (Tx[1,c+y_step]+Tx[1,c+y_step]+Tx[1,c+y_step+z_step]+Tx[1,c+y_step+z_step]-Tx[1,c]-Tx[1,c]-Tx[1,c+z_step]-Tx[1,c+z_step]))
                            b3 = ( 4 -(Tx[2,c+z_step]+Tx[2,c+z_step]+Tx[2,c+z_step+y_step]+Tx[2,c+z_step+y_step]-Tx[2,c]-Tx[2,c]-Tx[2,c+y_step]-Tx[2,c+y_step]))
                            b4 = ( 4 - (Tx[0,c]+Tx[0,c+y_step]+Tx[0,c+z_step]+Tx[0,c+y_step+z_step]-Tx[0,c]-Tx[0,c+y_step]-Tx[0,c+z_step]-Tx[0,c+y_step+z_step]))
                            b5 = (-(Tx[1,c+z_step]+Tx[1,c+z_step]+Tx[1,(c+z_step+y_step)]+Tx[1,(c+z_step+y_step)]-Tx[1,c]-Tx[1,c]-Tx[1,c+y_step]-Tx[1,c+y_step]))
                            b6 = (-(Tx[2,c+y_step]+Tx[2,c+y_step]+Tx[2,c+y_step+z_step]+Tx[2,c+y_step+z_step]-Tx[2,c]-Tx[2,c]-Tx[2,c+z_step]-Tx[2,c+z_step]))
                            b7 = (-(Tx[0,c+y_step]+Tx[0,c+y_step]+Tx[0,c+y_step+z_step]+Tx[0,c+y_step+z_step]-Tx[0,c]-Tx[0,c]-Tx[0,c+z_step]-Tx[0,c+z_step]))*(-(Tx[1,c]+Tx[1,c+y_step]+Tx[1,c+z_step]+Tx[1,c+y_step+z_step]-Tx[1,c]-Tx[1,c+y_step]-Tx[1,c+z_step]-Tx[1,c+y_step+z_step]))
                            b8 = ( 4 - (Tx[2,c+z_step]+Tx[2,c+z_step]+Tx[2,c+z_step+y_step]+Tx[2,(c+z_step+y_step)]-Tx[2,c]-Tx[2,c]-Tx[2,c+y_step]-Tx[2,c+y_step]))
                            b9 = (-(Tx[0,c+y_step]+Tx[0,c+y_step]+Tx[0,c+y_step+z_step]+Tx[0,c+y_step+z_step]-Tx[0,c]-Tx[0,c]-Tx[0,c+z_step]-Tx[0,c+z_step]))
                            b10 = (-(Tx[1,c+z_step]+Tx[1,c+z_step]+Tx[1,c+z_step+y_step]+Tx[1,c+z_step+y_step]-Tx[1,c]-Tx[1,c]-Tx[1,c+y_step]-Tx[1,c+y_step]))
                            b11 = ( -(Tx[2,c]+Tx[2,c+y_step]+Tx[2,c+z_step]+Tx[2,c+y_step+z_step]-Tx[2,c]-Tx[2,c+y_step]-Tx[2,c+z_step]-Tx[2,c+y_step+z_step]))
                            b12 = ( -(Tx[0,c+z_step]+Tx[0,c+z_step]+Tx[0,c+z_step+y_step]+Tx[0,c+z_step+y_step] -Tx[0,c]-Tx[0,c]-Tx[0,c+y_step]-Tx[0,c+y_step]))
                            b13 = (-(Tx[1,c]+Tx[1,c+y_step]+Tx[1,c+z_step]+Tx[1,(c+y_step+z_step)]-Tx[1,c] -Tx[1,c+y_step]-Tx[1,c+z_step]-Tx[1,c+y_step+z_step]))
                            b14 = (-(Tx[2,c+y_step]+Tx[2,c+y_step]+Tx[2,c+y_step+z_step]+Tx[2,c+y_step+z_step]-Tx[2,c]-Tx[2,c]-Tx[2,c+z_step]-Tx[2,c+z_step]))
                            b15 = (-(Tx[0,c+z_step]+Tx[0,c+z_step]+Tx[0,c+z_step+y_step]+Tx[0,c+z_step+y_step]-Tx[0,c]-Tx[0,c]-Tx[0,c+y_step]-Tx[0,c+y_step]))
                            b16 = (4-(Tx[1,c+y_step]+Tx[1,c+y_step]+Tx[1,c+y_step+z_step]+Tx[1,c+y_step+z_step]-Tx[1,c]-Tx[1,c]-Tx[1,c+z_step]-Tx[1,c+z_step])) *(-(Tx[2,c]+Tx[2,c+y_step]+Tx[2,c+z_step]+Tx[2,(c+y_step+z_step)]-Tx[2,c]-Tx[2,c+y_step]-Tx[2,c+z_step]-Tx[2,(c+y_step+z_step)]))
                            dv[c] = 0.015625 *( b1 * b2 *b3- b4 *b5 *b6 - b7  * b8 + b9  * b10  * b11 + b12 * b13 * b14- b15  * b16 )
                    
             
            drow = dv.reshape((s[0], s[1], s[2]))
            #swap back to matrix and resize to shape of volume
            simg = (141, 95, 87)
            drow_new = np.resize(drow, simg)
            
            drow_rs = drow_new.reshape(w,1)
          

            lobule_means_j = np.zeros((dist_vals,1))
            
            for val in range(dist_vals):
                doj_vals = drow_rs[idxlist[val]]
                lobule_means_j[val] = np.mean(doj_vals[~np.isnan(doj_vals)])
                
            a = sc*dist_vals #calculate index for where mean should be stored depending on which scan is being considered
            b = a + dist_vals
            data_j[p][a:b]  = np.transpose(lobule_means_j)
            
            count_files = count_files + 1
            
            
        else:
            
            print("File does not exist: ", TX_path)            

    
    if (not np.isnan(data_j[p,:]).all()) and (count_files > 1):
        
        for val in DV:
            DV_idxs = list(np.arange(val, (dist_vals * 5), dist_vals))
            DV_idxs_new = list(np.arange(val, (dist_vals * 7), dist_vals))

            #Load acquisition dates
            dates = list(dall.loc[p, dall.columns[0:5]].dt.days)
            data_temp = list(data_j[p,DV_idxs])
    
            for dt in range(len(dates)-1, -1, -1):
                if dates[dt] < 0 or np.isnan(data_temp[dt]):  #delete empty cells
                    del dates[dt]
                    del data_temp[dt]
           
        
             #Interpolate values from 'data_temp' acquired at 'dates' over new time sequence 'dates_new'
            if len(dates) > 0:
                z = np.polyfit(dates, data_temp, 3)  #fit polynomial of lobule means over original acquisition dates
                f = np.poly1d(z)

                dates_new = list(np.arange(30, 240, 30))  #establish desired image acquisition time sequence
                data_new = f(dates_new) #interpolate data over desired time sequence
            
                np.nan_to_num(data_new)
    
                data_int_j[counter,DV_idxs_new] = data_new
                group[counter] = PFS_specific[p]
        
        counter = counter+1
        
    else:
        data_int_j = np.delete(data_int_j, (counter), axis=0)
        group = np.delete(group, (counter), axis = 0)
        print("Patient deleted: ", p+1)
    
    count_files = 0

data_int_j[np.isnan(data_int_j)] = 0




from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import RFECV


svc = SVC(kernel="linear")
rfecv = RFECV(estimator=svc, step=1, cv=StratifiedKFold(6),
              scoring='accuracy')
rfecv.fit(data_int_j, group)
rfecv.ranking_


print("Optimal number of features : %d" % rfecv.n_features_)

# Plot number of features VS. cross-validation scores
plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score (nb of correct classifications)")
plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
plt.show()

search = np.array([1])
np.searchsorted(rfecv.ranking_, search)

top_feats = data_int_j[:,(rfecv.ranking_ == 1)]
feats_ind = np.where(rfecv.ranking_ < 5)
fi = feats_ind[0]


# ROC analysis

for run in range(4):
    
    y = group + 1
    
    if run == 0:
        X = data_int_j
        
    elif run == 1:
        X = top_feats
        
    elif run == 2:   
        X = top_feats[:,0]
        X = X.reshape(-1,1)

    elif run == 3:       
        X = top_feats[:,1]
        X = X.reshape(-1,1)


    perm = 100
    M = 5 #people left out

    Results = np.zeros((M,2)) #array to hold classification results


    minB = -50
    maxB = 50
    incB = 1
    B = len(range(minB,maxB,incB))

    TPF = np.zeros((B,perm))
    FPF = np.zeros((B,perm))
    tpf = np.zeros((B + 2,1))
    fpf = np.zeros((B + 2,1))


    cv = StratifiedKFold(n_splits=perm)
    classifier = SVC(kernel='linear')



    def new_predict(clf, new_bias, test_cohort):
        return np.sign(clf.decision_function(test_cohort) + clf.intercept_ - new_bias)

             
    for i in range(perm): #iterate through all possible 'leave M out' permutations
    
        d_train, d_test, g_train, g_test = train_test_split(X, y, test_size=(round(M/len(y),1)))
    
        classifier_fitted = classifier.fit(d_train, g_train)
        Results[:,0] = list(g_test)
        c=0 #array index
    
    
    
        for b in range(minB, maxB, incB):
        

        
            res_temp = new_predict(classifier_fitted, b, d_test)
            #res_temp = classifier_fitted.predict(d_test)

            Results[(res_temp == -1), 1] = 1
            Results[(res_temp == 1), 1] = 2
        
      
            F = Results[:,0] != Results[:,1] #find indicies of misclassified patients
            fp = Results[F,1] == 1 #find index of false positives
            FP = sum(fp) #no. of false positives

            fn = Results[F,1] == 0 #find index of false negatives
            FN = sum(fn) #no. of false negatives
 
            T = Results[:,1] == 1 #find indices of patients classified as positive
            t = sum(T) #find number of patients classified as positive
            TP = t - FP #number of true positives = total positives - false positives
 
            del T
            del t
        
            T = Results[:,1] == 0 #find indices of patients classified as negatives
            t = sum(T) #find number of patients classified as negative
            TN = t - FN #number of true negatives = total negatives - false negatives

 
 
            if (TP + FN)>0:
                TPF[c,i] = TP/(TP + FN) #divide true positives by total amount of positives to obtain true positive fraction
 
            if (FP + TN)>0:
                FPF[c,i] = FP/(FP + TN) #divide false positives by total amount of negatives to obtain false positive fraction

 
            c=c+1 #increment array index

    

    for i in range(B):
        tpf[i] = np.mean(TPF[i,:])
        fpf[i] = np.mean(FPF[i,:])
    
    tpf[B] = 1
    fpf[B] = 1
    tpf[B+1] = 0
    fpf[B+1] = 0

    tpf = np.sort(tpf, axis = 0)
    fpf = np.sort(fpf, axis = 0)


    if run == 0:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "All features, AUC = %0.2f" %(roc_auc))
        
    elif run == 1:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Top features, AUC = %0.2f" %(roc_auc))
        
    elif run == 2:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 1, AUC = %0.2f" %(roc_auc))
        
    elif run == 3:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 2, AUC = %0.2f" %(roc_auc))
        




plt.xlim([0, 1.05])
plt.ylim([0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve: SVM with leave-%d-out cross-validation Jacobian of Deformations' % (M))
plt.legend(loc="lower right")
plt.show()



###################################################################

##### plot features #####

import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np

suit_path = 'C:/Users/joshj/Desktop/Python_Test/CEREBELLA/SUIT.nii'
volume = nib.load(suit_path)
vol_data2 = volume.get_data()


for f in range(rfecv.n_features_):
    
    loc_ind = np.where(vol_data == (fi[f]%70))
    
    rows = loc_ind[0]
    cols = loc_ind[1]
    slices = loc_ind[2]
    month = math.ceil(fi[f] / 70)
    
    bm_cs = round(len(slices)/2) #cross-section of biomarker slice
    idx_bm_cs = np.where(slices == slices[bm_cs])

    Biomarker_slice = (vol_data2[:,:,slices[bm_cs]])

    plt.imshow(Biomarker_slice)
    plt.scatter(cols[idx_bm_cs],rows[idx_bm_cs], c='w') #plot mid-point of biomarker on cross-section of lobule
    plt.show()
    
    #plt.savefig(os.getcwd() + '\F' + str(f+1) + '_M'+ str(month)+ '_' '{}'.format(r'J.png'), bbox_inches='tight')
    
    data_VOI = np.zeros((vol_data2.shape))
    data_VOI[rows, cols, slices] = 1
    new_image = nib.Nifti1Image(data_VOI, (volume.affine))
    #nib.nifti1.save(new_image, os.getcwd() + '\F' + str(f+1) + '_M'+ str(month)+ '_' '{}'.format(r'J_LR.nii'))
