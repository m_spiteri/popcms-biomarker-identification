import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
import pandas as pd
import os
import svm
import math
from sklearn.model_selection import train_test_split
from sklearn.metrics import auc


root = 'C:/Users/mspiteri/Desktop/POPCMS/POPCMS_rep/CEREBELLA/'
suit_path = root + 'Atlases/Cerebellum-SUIT.nii' #path for labelled atlas
ds = pd.read_excel( root + 'DS.xlsx') #import acquisition date info for dataset



dates_all = { 'SA' : ds["A"] - ds["Date of Surg"] , 'SB' : ds["B"] - ds["Date of Surg"] , 'SC' : ds["C"] - ds["Date of Surg"] , 
              'SD' : ds["D"] - ds["Date of Surg"] , 'SE' : ds["E"] - ds["Date of Surg"] }

dall = pd.DataFrame(dates_all,columns= ['SA', 'SB', 'SC', 'SD', 'SE'])


volume = nib.load(suit_path)#load atlas
vol_data = volume.get_data()
dist_vals = 70 #number of distinct lobules within the posterior fossa

[rows,cols,slices] = vol_data.shape
w = rows*cols*slices #find total number of voxels in atlas


# splitting into left and right
zero_idxs = (vol_data == 0)
#originally atlas does not distinguish between left and right lobules and only contains 35 distinct labels.
#The volume is split into two and the labels on the left side are double in value, resulting in 70 distinct lobules
vol_data[0:70, :, :] = vol_data[0:70, :, :] + 35
vol_data[zero_idxs] = 0
#splitting over


vrow = vol_data.reshape(w,1) #vectorise atlas

idxlist = list();

# find distinct values
#DV = np.unique(vrow)
DV = range(dist_vals)
for val in DV:
    idx = np.where(vrow == val)
    idxlist.append(idx)


#Find group label
PFS_specific = ds["POPCMS"] #this is the class label (PFS and POPCMS are interchangeable terms)
 
months = 7 #analysis will be carried out over this amount of months
counter = 0
group = float('nan') * np.ones((38,1))
count_files = 0


scan_codes = ['A', 'B', 'C', 'D', 'E']
data = float('nan') * np.ones((48,(len(scan_codes)* dist_vals))) #allocate matrices for data
data_int = float('nan') * np.ones((48,(months* dist_vals)))

file_list = os.listdir(root + 'Registered Images/')
        
        
for p in range(48): #iterate through each patient
    for sc in range(len(scan_codes)): #iterate through each scan A to E
        
        keyword = 'wsuit_mf' + str(p+1) + scan_codes[sc]
        for fname in file_list:
            if keyword in fname:
                cer_path = root + 'Registered Images/' + fname #path to registered image
        
        if os.path.exists(cer_path):
            
            print("Processing image file: ", cer_path, " ...")
            
            # find lobule means for arbitrary subject
            cer_vol = nib.load(cer_path) #load registered image
            cer_data = cer_vol.get_data()
            crow = cer_data.reshape(w,1) #vectorise volume into single row vector
            
            lobule_means = np.zeros((dist_vals,1)) #where mean for each lobule will be stored
            
            for val in range(dist_vals):
                grey_level_vals = crow[idxlist[val]] #acquire all grey-level values within lobule "val"
                lobule_means[val] = np.mean(grey_level_vals[~np.isnan(grey_level_vals)]) #find lobule mean
                
            a = sc*dist_vals #calculate index for where mean should be stored depending on which scan is being considered
            b = a + dist_vals
            data[p][a:b]  = np.transpose(lobule_means)
            
            count_files = count_files + 1
            
        else:
            
            print("File does not exist: ", cer_path)
    
    
    if (not np.isnan(data[p,:]).all()) and (count_files > 1):
        
        for val in DV:
            DV_idxs = list(np.arange(val, (dist_vals * 5), dist_vals))
            DV_idxs_new = list(np.arange(val, (dist_vals * 7), dist_vals))

            #Load acquisition dates
            dates = list(dall.loc[p, dall.columns[0:5]].dt.days)
            data_temp = list(data[p,DV_idxs])
    
            for dt in range(len(dates)-1, -1, -1):
                if dates[dt] < 0 or np.isnan(data_temp[dt]) or np.isnan(dates[dt]): #delete empty cells
                    del dates[dt]
                    del data_temp[dt]
           
        
            #Interpolate values from 'data_temp' acquired at 'dates' over new time sequence 'dates_new'
            if len(dates) > 0:
                z = np.polyfit(dates, data_temp, 3) #fit polynomial of lobule means over original acquisition dates
                f = np.poly1d(z)

                dates_new = list(np.arange(30, 240, 30)) #establish desired image acquisition time sequence
                data_new = f(dates_new) #interpolate data over desired time sequence
            
                np.nan_to_num(data_new)
    
                data_int[counter,DV_idxs_new] = data_new
                group[counter] = PFS_specific[p]
        
        counter = counter+1
        
    else:
        data_int = np.delete(data_int, (counter), axis=0)
        print("Patient deleted: ", p+1)
    
    count_files = 0

data_int[np.isnan(data_int)] = 0


#feature selection
from sklearn.svm import SVC
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_selection import RFECV


svc = SVC(kernel="linear")
rfecv = RFECV(estimator=svc, step=1, cv=StratifiedKFold(6),
              scoring='accuracy')
rfecv.fit(data_int, group)
rfecv.ranking_


print("Optimal number of features : %d" % rfecv.n_features_)

# Plot number of features VS. cross-validation scores
plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score (nb of correct classifications)")
plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
plt.show()

search = np.array([1])
np.searchsorted(rfecv.ranking_, search)

top_feats = data_int[:,(rfecv.ranking_ == 1)]
feats_ind = np.where(rfecv.ranking_ == 1)
fi = feats_ind[0]


# ROC analysis

for run in range(5):
    
    y = group + 1
    
    if run == 0:
        X = data_int
        
    elif run == 1:
        X = top_feats

    elif run == 2:   
        X = top_feats[:,0]
        X = X.reshape(-1,1)

    elif run == 3:       
        X = top_feats[:,1]
        X = X.reshape(-1,1)

    elif run == 4:        
        X = top_feats[:,2]
        X = X.reshape(-1,1)


    perm = 100
    M = 4 #people left out

    Results = np.zeros((M,2)) #array to hold classification results


    minB = -50
    maxB = 50
    incB = 1
    B = len(range(minB,maxB,incB))

    TPF = np.zeros((B,perm))
    FPF = np.zeros((B,perm))
    tpf = np.zeros((B + 2,1))
    fpf = np.zeros((B + 2,1))


    cv = StratifiedKFold(n_splits=perm)
    classifier = svm.SVC(kernel='linear')



    def new_predict(clf, new_bias, test_cohort):
        return np.sign(clf.decision_function(test_cohort) + clf.intercept_ - new_bias)

             
    for i in range(perm): #iterate through all possible 'leave M out' permutations
    
        d_train, d_test, g_train, g_test = train_test_split(X, y, test_size=(round(M/len(y),1)))
    
        classifier_fitted = classifier.fit(d_train, g_train)
        Results[:,0] = list(g_test)
        c=0 #array index
    
    
    
        for b in range(minB, maxB, incB):
        

        
            res_temp = new_predict(classifier_fitted, b, d_test)
            #res_temp = classifier_fitted.predict(d_test)

            Results[(res_temp == -1), 1] = 1
            Results[(res_temp == 1), 1] = 2
        
      
            F = Results[:,0] != Results[:,1] #find indicies of misclassified patients
            fp = Results[F,1] == 1 #find index of false positives
            FP = sum(fp) #no. of false positives

            fn = Results[F,1] == 0 #find index of false negatives
            FN = sum(fn) #no. of false negatives
 
            T = Results[:,1] == 1 #find indices of patients classified as positive
            t = sum(T) #find number of patients classified as positive
            TP = t - FP #number of true positives = total positives - false positives
 
            del T
            del t
        
            T = Results[:,1] == 0 #find indices of patients classified as negatives
            t = sum(T) #find number of patients classified as negative
            TN = t - FN #number of true negatives = total negatives - false negatives

 
 
            if (TP + FN)>0:
                TPF[c,i] = TP/(TP + FN) #divide true positives by total amount of positives to obtain true positive fraction
 
            if (FP + TN)>0:
                FPF[c,i] = FP/(FP + TN) #divide false positives by total amount of negatives to obtain false positive fraction

 
            c=c+1 #increment array index

    

    for i in range(B):
        tpf[i] = np.mean(TPF[i,:])
        fpf[i] = np.mean(FPF[i,:])
    
    tpf[B] = 1
    fpf[B] = 1
    tpf[B+1] = 0
    fpf[B+1] = 0

    tpf = np.sort(tpf, axis = 0)
    fpf = np.sort(fpf, axis = 0)


    if run == 0:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "All features, AUC = %0.2f" %(roc_auc))
        
    elif run == 1:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Top 3 features, AUC = %0.2f" %(roc_auc))
        
    elif run == 2:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 1, AUC = %0.2f" %(roc_auc))
        
    elif run == 3:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 2, AUC = %0.2f" %(roc_auc))
        
    elif run == 4:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 3, AUC = %0.2f" %(roc_auc))
        




plt.xlim([0, 1.05])
plt.ylim([0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve: SVM with leave-%d-out cross-validation' % (M))
plt.legend(loc="lower right")
plt.show()


###################################################################

##### plot features #####

import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np

suit_path = 'C:/Users/joshj/Desktop/Python_Test/CEREBELLA/SUIT.nii'
volume = nib.load(suit_path)
vol_data2 = volume.get_data()


for f in range(rfecv.n_features_):
    
    loc_ind = np.where(vol_data == (fi[f]%70))
    
    rows = loc_ind[0]
    cols = loc_ind[1]
    slices = loc_ind[2]
    month = math.ceil(fi[f] / 70)
    
    bm_cs = round(len(slices)/2) #cross-section of biomarker slice
    idx_bm_cs = np.where(slices == slices[bm_cs])

    Biomarker_slice = (vol_data2[:,:,slices[bm_cs]])

    plt.imshow(Biomarker_slice)
    plt.scatter(cols[idx_bm_cs],rows[idx_bm_cs], c='w') #plot mid-point of biomarker on cross-section of lobule
    plt.show()
    
    plt.savefig(os.getcwd() + '\F' + str(f+1) + '_M'+ str(month)+ '_' '{}'.format(r'GL_LR.png'), bbox_inches='tight')
    
    
    data_VOI = np.zeros((vol_data2.shape))
    data_VOI[rows, cols, slices] = 1
    new_image = nib.Nifti1Image(data_VOI, (volume.affine))
    nib.nifti1.save(new_image, os.getcwd() + '\F' + str(f+1) + '_M'+ str(month)+ '_' '{}'.format(r'GL_LR.nii'))

