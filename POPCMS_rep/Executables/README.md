This GUI takes the following input parameters:

Templates path: 	This is the path to the SUIT labelled atlas and template (in .nii format)
Dataset path: 		This is the path to the dataset of images registered to the SUIT template.
			This folder must contain two sub-folders: one named "Registered Images" with 
			registered images in .nii format + the name containing the patient number 
			and MRI letter (ex name must contain wsuit_mf5A.....nii) and the other named 
			"Deformation Fields" with deformation fields saved in .mat and the name containing
			the patient number and MRI letter (ex name must contain "mf5A.......mat").
Patient Info Sheet:	.xlsx sheet including the following columns: 
			(1) Assignment No, (2) Hospital No, (3) Date of Pre-op, 
			(4) Date of Surgery (column MUST be named "Date of Surg")
			(5) - (9) Acquisition dates for images (columns must be labelled "A" to "E"), 
			(10) POPCMS label 1: YES, 0: NO (column MUST be named "POPCMS")

Report Path:		Path to save biomarker masks in .nii format, as a VOI on an image slice in .png
			and respective ROC curves

Following is some information about the related research and the code involved.



**** Registration and Segmentation ****

The POPCMS code is being developed to identify biomarkers from longitudinal datasets of brain 
MR of children who have undergone brain tumour resection. This analysis can be applied following
registration of the brain MR to a brain template + atlas. In this case the SUIT (spm) brain 
template was used and its corresponding labelled atlas Cerebellum-SUIT. These can both be found
in the folder "POPCMS_rep/CEREBELLA/Atlases". The atlas assigns a number from 0 to 34 to each
voxel in the posterior fossa to identify which lobule the voxel belongs to.

The MR images in the datasets were converted to NIfTI. They were then registered to the template
and the brain stem + cerebellum were segmented using the SPM toolbox + SUIT functions (process 
described in link below) using MATLAB.

The atlas + template, together with the registration and segmentation functions are found:
http://www.diedrichsenlab.org/imaging/suit.htm

**** Longitudinal Analysis ****

*Lobule Analysis*
The python file lobule_analysis.py loads each MR volume individually. In order to maintain info 
regarding whether lobule is on the left or right, each volume is split into two, this results in
70 separate numbered from 0 to 69 (as opposed to 0 to 34). The mean grey-level intensity for each
lobule is calculated. This is repeated for every MR volume longitudinally (on average, between 3
to 5 MR volumes have been acquired per subject). The MR volumes are not acquired at regular time 
intervals for each subject - for this reason a regular time sequence is established (for example
5 image acquisitions with 60 days in between each image acquisition) and the mean grey-level 
intensity (for each lobule) is extrapolated over this time sequence.
So for each subject we have 70 lobules and their mean grey-level over 5 (as an ex) time points.
Each data point is considered as a feature (or candidate biomarker).
We have 70x5 = 350 features in this example.


*Jacobian of Deformations Analysis* 
To analyse the deformations logntiduinally please see jacobian_lobule_analysis.py. Instead of 
analysing registered images, this code analyses the deformation field and calculates the 
determinant of jacobian of deformations for each deformation field (which is a result of the 
registration process). This data is also extrapolated as explained above.

Both scripts also includes feature selection, classification and biomarker plotting as explained 
below.

**** Feature Selection and Classification ****

Feature ranking (RFECV) is carried out using the scikit-learn module in python. This is done to 
rank features and identify how many features are required to correctly group subjects into
two groups (POPCMS or non-POPCMS). This is applied directly to the feature set.

Documentation for RFECV can be found here:
https://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.RFECV.html

Following this, moving bias ROC curves are plotted for each of the highest ranking features in
order to analyse their performance individually and collectively.


**** Plotting Biomarkers ****

Biomarkers can be plotted to screen (if using a python development environment) or can be saved 
as a NIfTI file and analysed in a NIfTI reader such as MRICRON.
