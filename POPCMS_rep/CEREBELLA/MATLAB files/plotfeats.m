rsfs = [1,22,80.00,5563;1,51,82.50,2530;5,49,87.50,3836];
rjsffs = [1,43,76.74,9474];
rsffs = [1,22,80.00,5563;2,22,80.00,5674];

data = rjsffs;

[s1 s2] = size(data);

for row = 1:s1

    S = data(row,4) + (13395 * (data(row,1)-1)) ;
    figure;
    idx1 = find(GRP == 1);
    idx2 = find(GRP == 2);
    plot(feats_matrixjac(idx1,S,data(row,2)), GRP(idx1), 'ob', 'MarkerFaceColor', 'b');
    hold on;
    plot(feats_matrixjac(idx2,S, data(row,2)), GRP(idx2), 'or', 'MarkerFaceColor', 'r');
    legend('NON-PFS','PFS')
    axis([-Inf Inf 0 3])
    title(strcat('SFS: Month:', num2str(data(row,1)), ' Slice:', ...
        num2str(data(row,2)), ' Index:', num2str(S), ' Mean Accuracy(%): ', ...
        num2str(data(row,1))));
        
    xlabel('Feature Intensity Value');
    ylabel('Class: NON-PFS = 1, PFS = 2');
end