function [new_space, Group] = longslice(slice, flag)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


DS = xlsread('/Users/michaelaspiteri/Documents/MATLAB/DS.xlsx');

[patients cols] = size(DS);

%max_days = max(max(DS(:,14)));
max_days = 210;
interval = 30;
start_date = 1;
ptsnums = 1:48;

slices = ceil(max_days/interval);

imgcount = zeros(95, 141);
count = 0;
w = 13395;
h = 1;

%data = zeros(slices,w);
data = zeros(95,141, 6);
dataq = zeros(slices, w);
n = numel(data);
Feature_Space = zeros(48, 95*141*ceil(max_days/interval));

scan = ['P','A', 'B', 'C', 'D', 'E'];
cd('/Users/michaelaspiteri/Desktop/CEREBELLA/')

suitnii = load_untouch_nii('SUIT.nii');
suit90 = rot90(suitnii.img(:,:,slice));
suit = fliplr(suit90);
template = im2bw(suit, graythresh(suit));
%cum_count = 0;
%cum_t = zeros(95,141);

for P = 1:patients
       
    pos = NaN * ones(1,6);
    for i=2:6

        if (i==1)
            img = strcat('wsuit_mf', num2str(P), 'PREOPnoT.nii');
        
        elseif (i==2)
            img = strcat('wsuit_mf', num2str(P), scan(i), 'res.nii');
        
        else
            img = strcat('wsuit_mf', num2str(P), scan(i), 'res.nii');
        end
        
        if exist(img, 'file')
            
        if (DS(P,8+i) >= 0)
                
        tx = load_untouch_nii(img);
        t90 = rot90(tx.img(:,:,slice));
        tlr = fliplr(t90);
        
       
        
        t = tlr.*template;
        

        pos(i) = DS(P,8+i);
        data(:,:, i) = t;
%        cum_t = cum_t + t;
%        cum_count = cum_count +1;

  
        else
        continue
        end
        
        else
        continue
        end
    
    end
 
    if sum(~isnan(pos))>1
    %Interpolate data against pos
    %DataInt = interp_data(data, pos, interval,1, max_days);
    %DataInt = interp_data(data, pos, interval,start_date, max_days);
    %DataInt = data;
    [s1 s2 s3] = size(data);
    
    for j = 1:s3
    n1 = numel(t);
    C = reshape(data(:,:,j), [1 n1]);
    dataq(j, :) = C;
    end
 
    else continue
    end
    
    n = numel(dataq);
    d = reshape(dataq', 1, n);
    Feature_Space(P,:) = d;

    data = zeros(95, 141, 6);
    dataq = zeros(slices, w);

end

%mean_img = cum_t/cum_count;
%n = numel(mean_img);
%M = reshape(mean_img, 1, n);


DS = xlsread('/Users/michaelaspiteri/Documents/MATLAB/DS.xlsx');
Group = DS(:,20);

idx1 = find(Group>0);
idx2= find(Group==0);

Group(idx1) = 2;
Group(idx2) = 1;


[S1 S2]= size(Feature_Space);

%somewhere here we are replicating the data
Features2 = Feature_Space;
Features2(:,S2+1) = 1:48;
%eliminate empty rows    

Features2(all(Features2(:,1:S2)==0,2),:)=[];
Group = Group(Features2(:,S2+1));
Features2(:,S2+1)=[];

% normalize and replace empty cells with NaN
[s1 s2] = size(Features2);

for P = 1:s1
for slc = 1:slices
    
col1 = (slc-1)*13395 + 1;
cols = col1: (col1+13394);
cells = find(Features2(P,cols) >0);

if(numel(cells) > 0)
    %Feature_Space(P,cols) = Feature_Space(P,cols) - M;
else
    Features2(P,cols) = zeros(1,13395);
end
end
end


new_space = Features2;
%[COEFF,SCORE,latent,tsquared,explained,mu1] = pca(Features2,'algorithm','als');

%new_space = SCORE*COEFF' + repmat(mu1,s1,1);
%new_space = SCORE(:,1:2)*COEFF(:,1:2)'+ repmat(mu1,s1,1);
%new_space = SCORE(:,1:2)*COEFF(:,1:2)';

if flag==1
p = 1; % position of first slice
q=1;

samples = s2/(141*95);
img = zeros(95*s1, 141*samples);


for i = 1:s1
startcol = 1;
q = 1;

for j = 1:samples
    
startcol = (j-1)*13395 + 1;
columns = startcol:(startcol + 13394);

A = reshape(new_space(i,columns), [95 141]);

rows = p:(p + 94);
cols = q:(q+140);

img(rows,cols) = A;

q = cols(end) + 1;

end
p = rows(end) + 1;

end


figure;
img = mat2gray(img, [0 500]);
imshow(img)
imwrite(img,strcat('long_noint_org',num2str(slice),'.png'))
end

end