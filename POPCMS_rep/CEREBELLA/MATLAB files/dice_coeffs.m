score = zeros(48,2);
count = 1;
%score(:,1) = 1:48;
cd('/Volumes/iPortable_Snow_x86/Berlin/REG/VOIs')


for i = 1:2
    
%    ref = load_untouch_nii('SL.nii');
    
    if i ==1
        s = 'L';
        ref = load_untouch_nii(strcat('Sfin', s, '.nii'));
        ref.img = flip(ref.img);

    elseif i == 2
        s = 'R';
        ref = load_untouch_nii(strcat('Sfin', s, '.nii'));
        ref.img = flip(ref.img);
    end
   
    %ref.img(ref.img>0) = 1;
 
        
    for p = 1:48
        
        path = strcat(num2str(p), 'B', s, '.nii');
        %path = strcat('wsuit_mf', num2str(p), 'Bres.nii');
        if exist(path)
        ION = load_untouch_nii(path);
        %ION.img(ION.img>0) = 1;
        %ION.img(isnan(ION.img)) = 0;
        common = (logical(ION.img) & logical(ref.img));
        a = sum(common(:));
        b = sum(ION.img(:));
        c = sum(ref.img(:));
        Dice = 2*a/(b+c);
        %figure;
        %imshowpair(reft(:,:,26), ION.img(:,:,26))
        score(p,1) = p;
        score(p,1+i) = Dice;
        end
        
    end
    
end

    z = find(score(:,1) == 0);
    score(z,:) =[];
    
    
    
for p =1:48
    
    path1 = strcat('mf', num2str(p), 'Bres_snc.mat');
    
    if exist(path1)
    load(path1);
    
    for i =1:2

    if i ==1
        s = 'L';
        ref = load_untouch_nii(strcat('Sfin', s, '.nii'));
        ref.img = flip(ref.img);

    elseif i == 2
        s = 'R';
        ref = load_untouch_nii(strcat('Sfin', s, '.nii'));
        ref.img = flip(ref.img);
    end
    
        path2 = strcat(num2str(p), 'B', s, '.nii');
        
        if exist(path2)
        ION = load_untouch_nii(path2);
        
        for d = 1:3
        Tr_1 = imresize(Tr(:,:,:,d), [141 95]);
        Tr_2 = permute(Tr_1, [1 3 2]);
        Tr_3 = imresize(Tr_2, [141 87]);
        Trr(:,:,:,d) = permute(Tr_3, [1 3 2]);
        end
        
        refw = imwarp(ref.img, Trr);
        
        
        common = (logical(ION.img) & logical(refw));
        a = sum(common(:));
        b = sum(ION.img(:));
        c = sum(refw(:));
        Dice = 2*a/(b+c);
        %figure;
        %imshowpair(refw(:,:,26), ION.img(:,:,26))
        score2(p,1) = p;
        score2(p,1+i) = Dice;
        end
    

    
    end
    end
end

    z = find(score2(:,1) == 0);
    score2(z,:) =[];