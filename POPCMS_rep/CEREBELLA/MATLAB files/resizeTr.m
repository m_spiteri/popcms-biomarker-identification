function [resliced_img] = resizeTr( img, dim_org, dim_new )
%dim_org are the original dimensions
%dim_new are the new image dimensions
%img takes the image matrix
%resliced_img returns the image in dim_new
c = 1;
resliced_img = zeros(dim_new);
zf = ceil(dim_new(3)/dim_org(3));

for counter = zf:zf:dim_new(3)
    resliced_img(:,:,counter) = imresize(img(:,:,c), [dim_new(1) dim_new(2)]);
    c = c+1;
end

end

