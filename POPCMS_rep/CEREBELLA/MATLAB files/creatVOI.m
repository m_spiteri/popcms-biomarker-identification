function [BMVOI] = creatVOI( bm, MRIseq )

maxcoord = MRIseq*13395;
idx1= bm <= maxcoord;
idx2 = bm > (maxcoord - 13395);

idxs = idx1 .* idx2;

bms = bm .* idxs;

BMVOI = zeros(141,95,87);
bmslice = zeros(95,141);

for slice = 1:60
    CELLS = bms(slice, 1:5);
    for cell = 1:5
        if CELLS(cell) > 0
            rows = ceil(CELLS(cell)/95);
            cols = rem(CELLS(cell), 95);
            bmslice(rows,cols) = 1;
        end
    end
    
    BMVOI(:,:,slice) = rot90(bmslice, 3);
    bmslice = zeros(95,141);
end


end

