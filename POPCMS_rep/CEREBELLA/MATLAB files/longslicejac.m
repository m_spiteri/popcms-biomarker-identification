function [new_space, Group] = longslicejac(slice, flag)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


DS = xlsread('/Users/michaelaspiteri/Documents/MATLAB/DS.xlsx');

[patients cols] = size(DS);

%max_days = max(max(DS(:,14)));
max_days = 210;
interval = 30;
start_date = 1;

slices = ceil(max_days/interval);

Tr = zeros(141, 95, 87,3);
w = 13395;

%data = zeros(slices,w);
data = zeros(95,141,6);
dataq = zeros(slices, w);
Feature_Space = zeros(48, 95*141*ceil(max_days/interval));

scan = ['P','A', 'B', 'C', 'D', 'E'];
cd('/Users/michaelaspiteri/Desktop/CEREBELLA/')


for P = 1:patients
       
    pos = NaN * ones(1,6);
    for i=1:6

        if (i==1)
            img = strcat('mf', num2str(P), 'PREOPnoT_snc.mat');
        
        elseif (i==2)
            img = strcat('mf', num2str(P), scan(i), 'res_snc.mat');
        
        else
            img = strcat('mf', num2str(P), scan(i), 'res_snc.mat');
        end
        
        if exist(img, 'file')
            
        if (DS(P,8+i) >= 0)
               
        Tx = load(img);

        size(Tx.Tr)
          for trslice = 1:3
            tr = resizeTr(Tx.Tr(:,:,:,trslice), size(Tx.Tr), [141 95 9]);
            trp = permute(tr, [3 2 1]);
            trz = resizeTr(trp, size(trp), [87 95 141]);
            Tr(:,:,:,trslice) = permute(trz, [3 2 1]);
          end
        
        
        T = jac3D(Tr);
        t = rot90(T(:,:,slice));
        t = fliplr(t);
        
        
        pos(i) = DS(P,8+i);
        data(:,:, i) = t;

  
        else
        continue
        end
        
        else
        continue
        end
    
    end
 
    if sum(~isnan(pos))>1
    %Interpolate data against pos
    DataInt = interp_data(data, pos, interval,start_date, max_days);
    
    %DataInt = data;
    [s1 s2 s3] = size(DataInt);
    
    for j = 1:s3
    n1 = numel(t);
    C = reshape(DataInt(:,:,j), [1 n1]);
    dataq(j, :) = C;
    end
 
    else continue
    end
    
    n = numel(dataq);
    d = reshape(dataq', 1, n);
    Feature_Space(P,:) = d;

    data = zeros(95, 141, 6);
    dataq = zeros(slices, w);

end


DS = xlsread('/Users/michaelaspiteri/Documents/MATLAB/DS.xlsx');
Group = DS(:,19);

idx1 = find(Group>0);
idx2= find(Group==0);

Group(idx1) = 2;
Group(idx2) = 1;


[S1 S2]= size(Feature_Space);

%somewhere here we are replicating the data
Features2 = Feature_Space;
Features2(:,S2+1) = 1:48;
%eliminate empty rows    

Features2(all(Features2(:,1:S2)==0,2),:)=[];
Group = Group(Features2(:,S2+1));
Features2(:,S2+1)=[];

% normalize and replace empty cells with NaN
[s1 s2] = size(Features2);

for P = 1:s1
for slice = 1:slices
    
col1 = (slice-1)*13395 + 1;
cols = col1: (col1+13395 - 1);
cells = find(Features2(P,cols) >0);

if(numel(cells) > 0)
    %Feature_Space(P,cols) = Feature_Space(P,cols) - M;
else
    Features2(P,cols) = NaN * ones(1,13395);
end
end
end



[COEFF,SCORE,latent,tsquared,explained,mu1] = pca(Features2,'algorithm','als');

new_space = SCORE(:,1:2)*COEFF(:,1:2)';

if flag==1
p = 1; % position of first slice

samples = s2/(13395);
img = zeros(95*s1, 141*samples);


for i = 1:s1
q = 1;

for j = 1:samples
    
startcol = (j-1)*13395 + 1;
columns = startcol:(startcol + 13395-1);

A = reshape(new_space(i,columns), [95 141]);

rows = p:(p + 94);
cols = q:(q + 140);

img(rows,cols) = A;

q = cols(end) + 1;

end
p = rows(end) + 1;

end


figure;
imshow(img, [ 0 500])
end

end