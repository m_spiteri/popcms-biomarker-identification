function [Acc] = SVMloocv(FtMatrix, Grp, feats)

C = cvpartition(Grp, 'LeaveOut');

err = zeros(C.NumTestSets,1);

for i = 1:C.NumTestSets
    trIdx = C.training(i);
    teIdx = C.test(i);
    
    S = svmtrain(FtMatrix(trIdx,feats), Grp(trIdx),'kernel_function', 'rbf');
    test = svmclassify(S, FtMatrix(teIdx,feats));
 
    
    if test ~= Grp(teIdx)
    err(i) = 1;
    end
end

cvErr = (sum(err)/C.NumTestSets)*100;
Acc = 100-cvErr;
end
 


