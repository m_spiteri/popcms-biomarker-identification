function [BMVOI, RES] = createVOI( bm, bmscores, accs, mask, MRIseq )

maxcoord = MRIseq*13395;
idx1= bm <= maxcoord;
idx2 = bm > (maxcoord - 13395);
idx3 = bmscores>= 75;
idx4 = accs >=75;

idxs = idx1 .* idx2 .*idx3 .* idx4;

[s1 s2] = size(bmscores);
RES = zeros(100, 8);



bms = (bm .* idxs) - (13395 * (MRIseq-1));


BMVOI = zeros(141,95,87); 
bmslice = zeros(95,141);
counter = 1;


for slice = 15:75
    mask_slice = mask(:,:,slice);
    CELLS = bms(slice, :);
    for cell = 1:s2
        if CELLS(cell) > 0 
            dupl = find(CELLS == CELLS(cell));
            r = numel(dupl);
            
            if r>1
            cols = ceil(CELLS(cell)/95);
            rows = rem(CELLS(cell), 95);
            
            d = (2*r) + 1;
            
            if (rows>d) && (cols>d) && (rows<92) && (cols<138)
                if mask_slice(rows, cols) == 1
            bmslice((rows-r):(rows+r),(cols-r):(cols+r)) = ones(d,d);
            RES(counter, 8) = CELLS(cell);
            RES(counter,7) = 1;
                end
                
            RES(counter,1) = slice;
            RES(counter,2) = 95 - rows;
            RES(counter,3) = cols;
            RES(counter,4) = r;
            RES(counter,5) = bmscores(slice,cell);
            RES(counter,6) = accs(slice,cell);
            counter = counter + 1;
            end
            end
            
            
        end
    end
    
 
    BMVOI(:,:,slice) = rot90(bmslice, -1);
    bmslice = zeros(95,141);
    
    BMVOI = uint8(BMVOI);
end


se = strel('cube',3);
BMVOI = imdilate(BMVOI,se);


end

