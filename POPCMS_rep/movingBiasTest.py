from scipy import interp
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import StratifiedKFold
import numpy as np
from sklearn.model_selection import train_test_split

for run in range(5):
    
    y = group + 1
    
    if run == 0:
        X = data_int
        
    elif run == 1:
        X = top_feats

    elif run == 2:   
        X = top_feats[:,0]
        X = X.reshape(-1,1)

    elif run == 3:       
        X = top_feats[:,1]
        X = X.reshape(-1,1)

    elif run == 4:        
        X = top_feats[:,2]
        X = X.reshape(-1,1)


    perm = 100
    M = 4 #people left out

    Results = np.zeros((M,2)) #array to hold classification results


    minB = -50
    maxB = 50
    incB = 1
    B = len(range(minB,maxB,incB))

    TPF = np.zeros((B,perm))
    FPF = np.zeros((B,perm))
    tpf = np.zeros((B + 2,1))
    fpf = np.zeros((B + 2,1))


    cv = StratifiedKFold(n_splits=perm)
    classifier = svm.SVC(kernel='linear')



    def new_predict(clf, new_bias, test_cohort):
        return np.sign(clf.decision_function(test_cohort) + clf.intercept_ - new_bias)

             
    for i in range(perm): #iterate through all possible 'leave M out' permutations
    
        d_train, d_test, g_train, g_test = train_test_split(X, y, test_size=(round(M/len(y),1)))
    
        classifier_fitted = classifier.fit(d_train, g_train)
        Results[:,0] = list(g_test)
        c=0 #array index
    
    
    
        for b in range(minB, maxB, incB):
        

        
            res_temp = new_predict(classifier_fitted, b, d_test)
            #res_temp = classifier_fitted.predict(d_test)

            Results[(res_temp == -1), 1] = 1
            Results[(res_temp == 1), 1] = 2
        
      
            F = Results[:,0] != Results[:,1] #find indicies of misclassified patients
            fp = Results[F,1] == 1 #find index of false positives
            FP = sum(fp) #no. of false positives

            fn = Results[F,1] == 0 #find index of false negatives
            FN = sum(fn) #no. of false negatives
 
            T = Results[:,1] == 1 #find indices of patients classified as positive
            t = sum(T) #find number of patients classified as positive
            TP = t - FP #number of true positives = total positives - false positives
 
            del T
            del t
        
            T = Results[:,1] == 0 #find indices of patients classified as negatives
            t = sum(T) #find number of patients classified as negative
            TN = t - FN #number of true negatives = total negatives - false negatives

 
 
            if (TP + FN)>0:
                TPF[c,i] = TP/(TP + FN) #divide true positives by total amount of positives to obtain true positive fraction
 
            if (FP + TN)>0:
                FPF[c,i] = FP/(FP + TN) #divide false positives by total amount of negatives to obtain false positive fraction

 
            c=c+1 #increment array index

    

    for i in range(B):
        tpf[i] = np.mean(TPF[i,:])
        fpf[i] = np.mean(FPF[i,:])
    
    tpf[B] = 1
    fpf[B] = 1
    tpf[B+1] = 0
    fpf[B+1] = 0

    tpf = np.sort(tpf, axis = 0)
    fpf = np.sort(fpf, axis = 0)


    if run == 0:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "All features, AUC = %0.2f" %(roc_auc))
        
    elif run == 1:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Top 3 features, AUC = %0.2f" %(roc_auc))
        
    elif run == 2:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 1, AUC = %0.2f" %(roc_auc))
        
    elif run == 3:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 2, AUC = %0.2f" %(roc_auc))
        
    elif run == 4:
        roc_auc = auc(fpf, tpf)
        plt.plot(fpf, tpf, label = "Feature 3, AUC = %0.2f" %(roc_auc))
        




plt.xlim([0, 1.05])
plt.ylim([0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('ROC curve: SVM with leave-%d-out cross-validation' % (M))
plt.legend(loc="lower right")
plt.show()